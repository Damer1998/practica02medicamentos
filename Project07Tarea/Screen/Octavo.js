
import React, { Component } from 'react';

import { StyleSheet, View, Text, 
  TouchableOpacity,
  Image,  Button } from 'react-native';

  const imageUrl = 'https://upload.wikimedia.org/wikipedia/commons/b/b3/Tasuku_Honjo_201311.jpg';

export default class Primera extends Component {
  constructor(props) {
    super(props);
    this.state = {

      
    };
    

  }
  static navigationOptions = {
    title: 'Tasuku Honjo',
    
  };


  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.listItem}  >

      <View style={styles.container}>
      <Image source={{uri:imageUrl}}  style={{width:300, height:200,borderRadius:20}} />

            <View style={{alignItems:"center",flex:1}}>
            
            <Text></Text>
              <Text style={styles.titulo}>Descripcion</Text>
      
              <Text style={styles.name} >Tasuku Honjo es un inmunólogo japonés, conocido por haber
               identificado la proteína 1 de la muerte celular
                programada Protein 1 </Text>
                 <Text style={styles.name} > Especialista en medicina General y farmacologia </Text>      
            </View>
     
        <Button title="Retroceder" onPress={() => navigate('SettingsScreen')} />
      </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

 

  titulo: {
    flex: 0.1,
    color: '#fafafa',
    fontWeight:"bold"
  },

  name: {
    flex: 0.3,
    color: '#fafafa'
  
  },



  listItem:{
    margin:12,
    padding:10,
    backgroundColor:"#512da8",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
